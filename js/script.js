  /*Document Ready*//////////////////////////////////////////////////////////////////////////////////////////////////////
jQuery(document).ready(function($) {
  'use strict';
  var fadeIn;
 
    fadeIn = function(screenshot) {
      return $('.section--active .features .iphone > img').each(function(i, el) {
        if ($(el).attr('data-screenshot') === screenshot)
        {
          $(el).addClass('screenshot-active');
          return;
        }
        return $(el).removeClass('screenshot-active');
      });
    };
    if ($('.health section').hasClass('section--active')){
      $('.features ul a').hover(function(e) {
      var $el, $list, screenshot;
      e.preventDefault();
      screenshot = $(e.currentTarget).attr('data-screenshot');
      $('.iphone').addClass('visible');
      fadeIn(screenshot);
      $el = $(e.currentTarget).parent();
      $list = $el.parent();
      $list.find('li').each(function(i, el) {
        return $(el).removeClass('active');
      });
      return $el.addClass('active');
    });
    }
   

/** @Fullscreen Height
    *************************************************** **/
    if(jQuery(".fullSlider").length > 0 || jQuery("#videoBg").length > 0 || jQuery(".video").length > 0) {
        _fullscreen();

        jQuery(window).resize(function() {
            _fullscreen();
        });
    }
   
    function _fullscreen() {
        

        var _headerHeight =$("header").height(); 
        var _screenHeight = jQuery(window).height() - _headerHeight;
        
        jQuery("#home, .image-caption, .image-caption .inner, #home.homeTop.imageOnly .image-caption, #home.homeTop img").height(_screenHeight);
        
}
    /** @FULLSCREEN SLIDER
*************************************************** **/
    function fullSlider(sliderContainer) {

        if(jQuery(sliderContainer + " .fullSlider").length) {
            jQuery(sliderContainer + " .fullSlider").maximage({
                cycleOptions: {
                    fx:         'fade',
                    auto: 'true ',
                    speed:      1000,
                    adaptiveHeight: 'true',
                    autoStart: 'true',
                    prev:       sliderContainer + ' .sliderPrev',
                    next:       sliderContainer + ' .sliderNext',
                    pause:      1,

                    before: function(last,current){
                        jQuery('.image-caption').fadeOut().animate({top:'100px'},{queue:true, easing: 'easeOutQuad', duration: 550});
                        jQuery('.image-caption').fadeOut().animate({top:'-100px'});
                    },

                    after: function(last,current){
                        jQuery('.image-caption').fadeIn().animate({top:'0'},{queue:true, easing: 'easeOutQuad', duration: 450});
                    }   
                },
             
                onFirstImageLoaded: function(){
                    jQuery(sliderContainer + ' .imgLoader').delay(800).hide();
                    jQuery('.fullSlider').delay(300).fadeIn('slow');
                    jQuery('.image-caption').fadeIn().animate({top:'0'});       
                }
            });

        }

        // no click
        jQuery(sliderContainer + ' .sliderPrev , ' + sliderContainer + ' .sliderNext').bind("click", function(e) {
            e.preventDefault();
        });

    }


    // HOME FullSlider
    fullSlider("#home");

    // Content FullSlider
    fullSlider(".contentFullSlider");



/** @BXSLIDER
*************************************************** **/
    function bxsliderInit(divSlider) {

        if(jQuery().bxSlider) {

            jQuery(divSlider).bxSlider({
                controls: true, 
                pager: false,       
                auto:true,
                mode: 'fade',
                speed: 800,
                pause: 1000,
                preloadImages:'all'
            });

        }

    }   bxsliderInit('.bxslider');

});
